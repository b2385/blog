import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import ToastMsg from './ToastMsg'

import Cookies from "js-cookie";
import Link from 'next/link'
import ScrollToTop from './ScrollToTop'
import LoginModal from "./LoginModal";
import { socket } from '../helper/auth'

import { Menu, Dropdown, List, Avatar, Badge } from 'antd';
import moment from "moment";
import axios from "axios";

export default function NavbarComp() {
  const [showLoginModal, setShowLoginModal] = useState(false)
  const [query, setQuery] = useState(null)

  let { user, authenticated, profile } = useSelector((state) => state.auth);
  let { chats } = useSelector((state) => state.chat);
  let { notifications } = useSelector((state) => state.notification);
  let dispatch = useDispatch();
  let Router = useRouter()


  let logOut = () => {
    Cookies.remove("flikhs_auth");
    dispatch({
      type: "LOGOUT",
    });
    window.location.pathname = "/";
  };




  useEffect(() => {
    if (socket) {
      socket.on("newmessage", data => {
        console.log(data);
        dispatch({
          type: "UPDATE_CHATS",
          payload: data.chat
        })
      })

      socket.on("newnotification", data => {
        console.log(data);
        dispatch({
          type: "NEW_NOTIFICATION",
          payload: data.notification
        })
      })
    }
  }, [socket])


  useEffect(() => {
    if (query !== null) {
      const delayed = setTimeout(() => {
        Router.push(`/?query=${query}`)
      }, 300)
      return () => clearTimeout(delayed)
    }

  }, [query])

  const findOtherUser = (users) => {
    let otherUser = users.filter(u => u._id !== profile._id)[0]
    return otherUser
  }

  const messageLists = (
    <List
      // header={<p>Messages</p>}
      style={{ width: "400px", maxHeight: "50vh", overflowY: "auto", padding: "0" }}
      size="large"
      itemLayout="horizontal"
      dataSource={chats}
      renderItem={chat => (
        <List.Item className={chat.toRead.includes(profile._id) ? "unread" : ""} onClick={() => Router.push(`/chat/${chat._id}`)} style={{ cursor: "pointer" }}>
          <List.Item.Meta
            avatar={<Avatar src={findOtherUser(chat.users).profileimg} />}
            title={<span>{findOtherUser(chat.users).first} {findOtherUser(chat.users).last}</span>}
            description={
              !chat.latestMessage ? <>New chat</> :
                chat.latestMessage?.type === 'image' ?
                  <>
                    {chat.latestMessage.sender.first}: Image
                  </>
                  :
                  <>
                    {chat.latestMessage.sender.first}: {chat.latestMessage.content}
                  </>
            }
          />
        </List.Item>
      )}
    />
  );



  const generateNotificationText = (type, user) => {
    let text = ""
    if (type === "like") {
      text = `${user.first} ${user.last} liked your post`
    }
    if (type === "comment") {
      text = `${user.first} ${user.last} commented on your post`
    }
    return text
  }


  const generateNotificationUrl = (type, entityId) => {
    let url = ""
    if (type === "like") {
      url = `/post/${entityId}`
    }
    if (type === "comment") {
      url = `/post/${entityId}`
    }
    return url
  }


  const notificationMarkRead = (noti) => {
    axios.patch(`/notification/markread/${noti._id}`)
      .then(res => {
        dispatch({
          type: "UPDATE_NOTIFICATION",
          payload: res.data.notification
        })
        //console.log(res.data.notification);
        Router.push(generateNotificationUrl(noti.notificationType, noti.entityId))
      })
      .catch(err => {
        console.log(err);
      })
  }


  const notificationLists = (
    <List
      // header={<p>Messages</p>}
      style={{ width: "400px", maxHeight: "50vh", overflowY: "auto", padding: "0" }}
      size="small"
      itemLayout="horizontal"
      dataSource={notifications}
      renderItem={noti => (
        <List.Item onClick={() => notificationMarkRead(noti)} className={!noti.opened ? "unread" : ""} style={{ cursor: "pointer" }}>
          <List.Item.Meta
            avatar={<Avatar src={noti.userFrom.profileimg} />}
            title={<span >{generateNotificationText(noti.notificationType, noti.userFrom)}</span>}
            description={moment(noti.createdAt).fromNow()}
          />

        </List.Item>
      )}
    />
  );

  const barContents = (
    <>
      <div
        className="dropdown_bar"
        style={{ width: "200px", borderRadius: "5px" }}
      >
        <p className='title'>MY FEEDS</p>
        <Link href="/">
          <a className="item"><i className="fas fa-home"></i>Home</a>
        </Link>
        <li>
          <Link href="/articles">
            <a className="item"><i className="fas fa-chart-line"></i>Articles</a>
          </Link>
        </li>
        <li>
          <Link href="/blogs">
            <a className="item"><i className="fas fa-chart-bar"></i>Blogs</a>
          </Link>
        </li>
        <li>
          <Link href="/groups">
            <a className="item"><i className="fas fa-list-ol"></i>Groups</a>
          </Link>
        </li>
        {authenticated && <>
          <p className='title'>Shortcuts</p>

          {
            profile && profile.pinnedgroups && profile.pinnedgroups.length > 0 && profile.pinnedgroups.map((group, index) => {
              return <li key={index}>
                <Link href={`/g/${group.slug}`}>
                  <a className="item">
                    <img src={group.groupimg ? group.groupimg : '/groupholder.png'} alt="" className="img" />
                    {group.name}
                  </a>
                </Link>
              </li>
            })
          }

          {

            profile && profile.pinnedblogs && profile.pinnedblogs.length > 0 && profile.pinnedblogs.map((blog, index) => {
              return <li key={index}>
                <Link href={`/b/${blog.slug}`}>
                  <a className="item">
                    <img src={blog.blogImage ? blog.blogImage : '/groupholder.png'} alt="" className="img" />
                    {blog.name}
                  </a>
                </Link>
              </li>
            })

          }

          {
            profile && profile.pinnedgroups && profile.pinnedgroups.length > 0 ? null :
              profile && profile.pinnedblogs && profile.pinnedblogs.length > 0 ? null :
                <li className="item">No shourts fround</li>
          }




          <p className='title'>Other</p>
          <li>
            <Link href="/user/setting">
              <a className="item"><i className="fas fa-user-cog"></i>User settings</a>
            </Link>
          </li>
          <li>
            <a className="item" href="#">
              <i className="fas fa-envelope"></i>
              Messege
            </a>
          </li>
          <li>
            <a className="item" href="#">
              <i className="fas  fa-edit"></i>Create post
            </a>
          </li>
        </>
        }
      </div>
    </>
  )



  const userDropdown = (
    <>
      <div className='user_drop'>
        <Link href="/profile">
          <a className="item"><i className="fas fa-user-circle"></i> My profile</a>
        </Link>
        <Link href="/user/setting">
          <a className="item"><i className="fas fa-user-cog"></i> User Setting</a>
        </Link>
        <span onClick={() => logOut()} className="item logout">
          <i className="fas fa-sign-out-alt "></i> Logout
        </span>
      </div>
    </>
  )

  return (
    <>
      <LoginModal show={showLoginModal} onCloseModal={() => setShowLoginModal(false)} />
      <ToastMsg />
      <ScrollToTop />
      <header className="header_part shadow-sm ">
        <div className="default_container ">
          <nav className="header_content">
            <Link href="/">
              <a className="brand" >
                <img src='/logo.png' alt="" />
                {/* <img src='/logo.png' alt="" className="mobile_logo" /> */}
              </a>
            </Link>

            <Dropdown overlay={barContents} getPopupContainer={triggerNode => triggerNode.parentElement} trigger={['click']}>

              <a
                className="nav-link dropdown-toggle"

              >
                <i style={{ color: "white" }} className="fas fa-bars"></i>
              </a>


            </Dropdown>


            <input
              className="nav_search"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />



            {authenticated && (
              <>

                <Link href="/profile">

                  <a className="nav_profile">
                    <img src={profile.profileimg || "profile.png"} alt="" />{" "}
                    <span></span>{profile.first} {profile.last}
                  </a>
                </Link>



                <ul className="navbar-nav menu mobile-res">
                  <li className="nav-item">

                    <Dropdown getPopupContainer={triggerNode => triggerNode.parentElement} overlay={messageLists} trigger={['click']}>
                      <a className="ant-dropdown-link nav_icon mobile-res" onClick={e => e.preventDefault()}>
                        <Badge count={chats && chats.filter(chat => chat.toRead.includes(profile._id)).length}>
                          <i className="fas fa-comment-dots icon"></i>
                        </Badge>

                      </a>
                    </Dropdown>

                  </li>
                </ul>


                <ul className="navbar-nav menu mobile-res">
                  <li className="nav-item">

                    <Dropdown getPopupContainer={triggerNode => triggerNode.parentElement} overlay={notificationLists} trigger={['click']}>
                      <a className="ant-dropdown-link nav_icon mobile-res" onClick={e => e.preventDefault()}>
                        <Badge count={notifications && notifications.filter(noti => !noti.opened).length}>
                          <i className="fas fa-bell icon"></i>
                        </Badge>

                      </a>
                    </Dropdown>

                  </li>
                </ul>


              </>
            )}


            {authenticated ? <div className="dropdown menu">
              <Dropdown overlay={userDropdown} getPopupContainer={triggerNode => triggerNode.parentElement} trigger={['click']}>
                <span>
                  <i className="fas fa-caret-down icon round"></i>
                </span>
              </Dropdown>


            </div> :
              <Link href='/login'>
                <a className='login_button'>Login</a>
              </Link>
            }

          </nav>
        </div>
      </header>

    </>
  );
}
